import { KeycloakConfig } from 'keycloak-angular';

let Credentials = {
  secret: "584ace77-44bd-482b-b990-05be8d81466a"
};

let keycloakConfig: KeycloakConfig = {
  url: 'http://localhost:8080/auth',
  realm: 'stack-realm',
  clientId: 'stack',
  credentials: Credentials
};

let appConfig = {
  url: 'http://localhost:4101'
};

export const environment = {
  production: false,
  keycloak: keycloakConfig,
  appConfig: appConfig
};


