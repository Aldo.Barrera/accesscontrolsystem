import { KeycloakConfig } from 'keycloak-angular';

let Credentials = {
  secret: "63cd6a5b-ed78-4bc3-86aa-7cd1ce7ea5c2"
};

let keycloakConfig: KeycloakConfig = {
  url: 'https://192.168.100.6:8443/auth',
  realm: 'stack-production-realm',
  clientId: 'stack-production',
  credentials: Credentials
};

let appConfig = {
  url: 'https://192.168.100.6:4000'
};

export const environment = {
  production: false,
  keycloak: keycloakConfig,
  appConfig: appConfig
};

