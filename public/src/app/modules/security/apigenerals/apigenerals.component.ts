import { Component, Input } from '@angular/core';
import {CommonsComponent} from "../../../common/commons.component";
import {default as config} from '../config/config.json';
import {ApiGeneralsService} from "./shared/apigenerals.service";

@Component({
  selector: 'app-' + config['apiGenerals'].component.nameModule.toLowerCase() ,
  templateUrl: './apigenerals.component.html',
  styleUrls: ['./apigenerals.component.css']
})
export class ApiGeneralsComponent extends CommonsComponent {

  @Input() datafromadd: any[] = [];
  constructor(private apiGeneralsService: ApiGeneralsService) { 
    super(apiGeneralsService);
    this.moduleName = config['apiGenerals'].component.nameModule  ;
	this.modulePref = config['apiGenerals'].component.prefix  ;
	this.keyName =  "_id";
  }
  ngOnInit() {
    
    super.ngOnInit(); 
  }
}
