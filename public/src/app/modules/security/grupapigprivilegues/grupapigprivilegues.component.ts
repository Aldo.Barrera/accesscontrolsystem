import { Component, Input } from '@angular/core';
import {CommonsComponent} from "../../../common/commons.component";
import {default as config} from '../config/config.json';
import {GrupApigPrivileguesService} from "./shared/grupapigprivilegues.service";

@Component({
  selector: 'app-' + config['grupApigPrivilegues'].component.nameModule.toLowerCase() ,
  templateUrl: './grupapigprivilegues.component.html',
  styleUrls: ['./grupapigprivilegues.component.css']
})
export class GrupApigPrivileguesComponent extends CommonsComponent {

  @Input() datafromadd: any[] = [];
  constructor(private grupApigPrivileguesService: GrupApigPrivileguesService) { 
    super(grupApigPrivileguesService);
    this.moduleName = config['grupApigPrivilegues'].component.nameModule  ;
	this.modulePref = config['grupApigPrivilegues'].component.prefix  ;
	this.keyName =  "_id";
  }
  ngOnInit() {
    
    super.ngOnInit(); 
  }
}
