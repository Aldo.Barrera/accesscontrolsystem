import { Component, Input } from '@angular/core';
import {CommonsComponent} from "../../../common/commons.component";
import {default as config} from '../config/config.json';
import {EmployeesService} from "./shared/employees.service";

@Component({
  selector: 'app-' + config['employees'].component.nameModule.toLowerCase() ,
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent extends CommonsComponent {

  @Input() datafromadd: any[] = [];
  constructor(private employeesService: EmployeesService) { 
    super(employeesService);
    this.moduleName = config['employees'].component.nameModule  ;
	this.modulePref = config['employees'].component.prefix  ;
	this.keyName =  "_id";
  }
  ngOnInit() {
    
    super.ngOnInit(); 
  }
}
