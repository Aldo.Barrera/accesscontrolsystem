﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsDetailComponent } from '../../../../common/commons-detail/commons-detail.component';

import {default as config} from '../../config/config.json';
import { EmployeesService } from '../shared/employees.service';

@Component({
  selector: 'app-' + config['employees'].component.nameModule.toLowerCase() + '-form',
  templateUrl: './employees-detail.component.html',
  styleUrls: ['./employees-detail.component.css']
})
export class EmployeesDetailComponent extends CommonsDetailComponent {
  
  constructor(router: Router, route: ActivatedRoute, employeesService: EmployeesService) 
  {
    super(router, route, employeesService);
    this.moduleName = config['employees'].component.nameModule ;
	this.modulePref = config['employees'].component.prefix  ;
  }
}
