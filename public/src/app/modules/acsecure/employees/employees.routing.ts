﻿import { Routes, RouterModule } from '@angular/router';
import {default as config} from '../config/config.json';
import { EmployeesComponent } from './employees.component';
import {EmployeesDetailComponent} from "./employees-detail/employees-detail.component";
import {EmployeesAddComponent} from "./employees-add/employees-add.component";
import {EmployeesEditComponent} from "./employees-edit/employees-edit.component";
import {EmployeesSearchComponent} from "./employees-search/employees-search.component";

const employeesRoutes: Routes = [

    { path: config['employees'].component.nameModule.toLowerCase() , component: EmployeesComponent, pathMatch: 'full' },
    { path: config['employees'].component.nameModule.toLowerCase()  + '/:id', component: EmployeesDetailComponent},
    { path: config['employees'].component.nameModule.toLowerCase()  + 'add', component: EmployeesAddComponent},
    { path: config['employees'].component.nameModule.toLowerCase() + 'edit/:id', component: EmployeesEditComponent},
	{ path: config['employees'].component.nameModule.toLowerCase() + 'search', component: EmployeesSearchComponent}
];

export const employeesRouting = RouterModule.forChild(employeesRoutes);
