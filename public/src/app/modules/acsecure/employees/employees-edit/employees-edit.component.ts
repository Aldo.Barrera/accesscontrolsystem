﻿import { Component  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsEditComponent } from '../../../../common/commons-edit/commons-edit.component';

import {default as config} from '../../config/config.json';


import { EmployeesService } from '../shared/employees.service';
import { FormsService } from '../../../../dynamicforms/forms/shared/forms.service';

@Component({
  selector: 'app-' + config['employees'].component.nameModule.toLowerCase()  + '-addform',
  templateUrl: './employees-edit.component.html',
  styleUrls: ['./employees-edit.component.css'],
  providers:  [FormsService]
})
export class EmployeesEditComponent extends CommonsEditComponent {
 
  constructor(router: Router, route: ActivatedRoute, employeesService: EmployeesService, formsService: FormsService) 
  {    
    super(router, route, employeesService, formsService);    
    this.moduleName = config['employees'].component.nameModule;
	this.modulePref = config['employees'].component.prefix  ;
	this.keyName = "_id"  ;
	this.components = config;
  }

  createForm() {
    this.controlsJson = config['employees'].model;
	  this.components = config;
    super.createForm();  
  }
  
}
