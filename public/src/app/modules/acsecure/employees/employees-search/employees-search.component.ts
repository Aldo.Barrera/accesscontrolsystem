﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsSearchComponent } from '../../../../common/commons-search/commons-search.component';

import {default as config} from '../../config/config.json';
import { EmployeesService } from '../shared/employees.service';

@Component({
  selector: 'app-' + config['employees'].component.nameModule.toLowerCase() + '-form',
  templateUrl: './employees-search.component.html',
  styleUrls: ['./employees-search.component.css']
})
export class EmployeesSearchComponent extends CommonsSearchComponent {
  
  constructor(router: Router, route: ActivatedRoute, employeesService: EmployeesService) 
  {
    super(router, route, employeesService);
    this.moduleName = config['employees'].component.nameModule ;
	this.modulePref = config['employees'].component.prefix  ;
  }
}
