﻿import { Component} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsAddComponent } from '../../../../common/commons-add/commons-add.component';

import {default as config} from '../../config/config.json';

import { EmplCleaRelationService } from '../shared/emplclearelation.service';
import { FormsService } from '../../../../dynamicforms/forms/shared/forms.service';

@Component({
  selector: 'app-' + config['emplCleaRelation'].component.nameModule.toLowerCase()  + '-addform',
  templateUrl: './emplclearelation-add.component.html',
  styleUrls: ['./emplclearelation-add.component.css'],
  providers:  [FormsService]
})
export class EmplCleaRelationAddComponent extends CommonsAddComponent {

  constructor(router: Router, route: ActivatedRoute, emplCleaRelationService: EmplCleaRelationService, formsService: FormsService) 
  {    
    super(router, route, emplCleaRelationService, formsService);    
    this.moduleName = config['emplCleaRelation'].component.nameModule;
	this.modulePref = config['emplCleaRelation'].component.prefix;
	this.keyName = "_id";
	this.components = config;
  }

  createForm() {
    this.controlsJson = config['emplCleaRelation'].model;    
	this.components = config;
    super.createForm();    
  }
}
