﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsSearchComponent } from '../../../../common/commons-search/commons-search.component';

import {default as config} from '../../config/config.json';
import { EmplCleaRelationService } from '../shared/emplclearelation.service';

@Component({
  selector: 'app-' + config['emplCleaRelation'].component.nameModule.toLowerCase() + '-form',
  templateUrl: './emplclearelation-search.component.html',
  styleUrls: ['./emplclearelation-search.component.css']
})
export class EmplCleaRelationSearchComponent extends CommonsSearchComponent {
  
  constructor(router: Router, route: ActivatedRoute, emplCleaRelationService: EmplCleaRelationService) 
  {
    super(router, route, emplCleaRelationService);
    this.moduleName = config['emplCleaRelation'].component.nameModule ;
	this.modulePref = config['emplCleaRelation'].component.prefix  ;
  }
}
