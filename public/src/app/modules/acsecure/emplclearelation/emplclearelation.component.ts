import { Component, Input } from '@angular/core';
import {CommonsComponent} from "../../../common/commons.component";
import {default as config} from '../config/config.json';
import {EmplCleaRelationService} from "./shared/emplclearelation.service";

@Component({
  selector: 'app-' + config['emplCleaRelation'].component.nameModule.toLowerCase() ,
  templateUrl: './emplclearelation.component.html',
  styleUrls: ['./emplclearelation.component.css']
})
export class EmplCleaRelationComponent extends CommonsComponent {

  @Input() datafromadd: any[] = [];
  constructor(private emplCleaRelationService: EmplCleaRelationService) { 
    super(emplCleaRelationService);
    this.moduleName = config['emplCleaRelation'].component.nameModule  ;
	this.modulePref = config['emplCleaRelation'].component.prefix  ;
	this.keyName =  "_id";
  }
  ngOnInit() {
    
    super.ngOnInit(); 
  }
}
