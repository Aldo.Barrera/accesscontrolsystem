﻿import { Component  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsEditComponent } from '../../../../common/commons-edit/commons-edit.component';

import {default as config} from '../../config/config.json';


import { EmplCleaRelationService } from '../shared/emplclearelation.service';
import { FormsService } from '../../../../dynamicforms/forms/shared/forms.service';

@Component({
  selector: 'app-' + config['emplCleaRelation'].component.nameModule.toLowerCase()  + '-addform',
  templateUrl: './emplclearelation-edit.component.html',
  styleUrls: ['./emplclearelation-edit.component.css'],
  providers:  [FormsService]
})
export class EmplCleaRelationEditComponent extends CommonsEditComponent {
 
  constructor(router: Router, route: ActivatedRoute, emplCleaRelationService: EmplCleaRelationService, formsService: FormsService) 
  {    
    super(router, route, emplCleaRelationService, formsService);    
    this.moduleName = config['emplCleaRelation'].component.nameModule;
	this.modulePref = config['emplCleaRelation'].component.prefix  ;
	this.keyName = "_id"  ;
	this.components = config;
  }

  createForm() {
    this.controlsJson = config['emplCleaRelation'].model;
	  this.components = config;
    super.createForm();  
  }
  
}
