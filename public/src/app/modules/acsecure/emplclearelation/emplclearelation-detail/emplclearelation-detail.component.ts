﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsDetailComponent } from '../../../../common/commons-detail/commons-detail.component';

import {default as config} from '../../config/config.json';
import { EmplCleaRelationService } from '../shared/emplclearelation.service';

@Component({
  selector: 'app-' + config['emplCleaRelation'].component.nameModule.toLowerCase() + '-form',
  templateUrl: './emplclearelation-detail.component.html',
  styleUrls: ['./emplclearelation-detail.component.css']
})
export class EmplCleaRelationDetailComponent extends CommonsDetailComponent {
  
  constructor(router: Router, route: ActivatedRoute, emplCleaRelationService: EmplCleaRelationService) 
  {
    super(router, route, emplCleaRelationService);
    this.moduleName = config['emplCleaRelation'].component.nameModule ;
	this.modulePref = config['emplCleaRelation'].component.prefix  ;
  }
}
