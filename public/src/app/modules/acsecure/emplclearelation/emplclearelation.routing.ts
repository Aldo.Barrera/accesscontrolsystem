﻿import { Routes, RouterModule } from '@angular/router';
import {default as config} from '../config/config.json';
import { EmplCleaRelationComponent } from './emplclearelation.component';
import {EmplCleaRelationDetailComponent} from "./emplclearelation-detail/emplclearelation-detail.component";
import {EmplCleaRelationAddComponent} from "./emplclearelation-add/emplclearelation-add.component";
import {EmplCleaRelationEditComponent} from "./emplclearelation-edit/emplclearelation-edit.component";
import {EmplCleaRelationSearchComponent} from "./emplclearelation-search/emplclearelation-search.component";

const emplCleaRelationRoutes: Routes = [

    { path: config['emplCleaRelation'].component.nameModule.toLowerCase() , component: EmplCleaRelationComponent, pathMatch: 'full' },
    { path: config['emplCleaRelation'].component.nameModule.toLowerCase()  + '/:id', component: EmplCleaRelationDetailComponent},
    { path: config['emplCleaRelation'].component.nameModule.toLowerCase()  + 'add', component: EmplCleaRelationAddComponent},
    { path: config['emplCleaRelation'].component.nameModule.toLowerCase() + 'edit/:id', component: EmplCleaRelationEditComponent},
	{ path: config['emplCleaRelation'].component.nameModule.toLowerCase() + 'search', component: EmplCleaRelationSearchComponent}
];

export const emplCleaRelationRouting = RouterModule.forChild(emplCleaRelationRoutes);
