﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule }  from '@angular/router';
import { HttpClientModule }  from '@angular/common/http';

import { EmployeesComponent } from "./employees/employees.component"; 
import { EmployeesService } from "./employees/shared/employees.service"; 
import { EmployeesDetailComponent } from "./employees/employees-detail/employees-detail.component"; 
import { EmployeesAddComponent } from "./employees/employees-add/employees-add.component"; 
import { EmployeesEditComponent } from "./employees/employees-edit/employees-edit.component"; 
import { EmployeesSearchComponent } from "./employees/employees-search/employees-search.component"; 
import { employeesRouting } from "./employees/employees.routing"; 
import { ClearancesComponent } from "./clearances/clearances.component"; 
import { ClearancesService } from "./clearances/shared/clearances.service"; 
import { ClearancesDetailComponent } from "./clearances/clearances-detail/clearances-detail.component"; 
import { ClearancesAddComponent } from "./clearances/clearances-add/clearances-add.component"; 
import { ClearancesEditComponent } from "./clearances/clearances-edit/clearances-edit.component"; 
import { ClearancesSearchComponent } from "./clearances/clearances-search/clearances-search.component"; 
import { clearancesRouting } from "./clearances/clearances.routing"; 
import { DoorsComponent } from "./doors/doors.component"; 
import { DoorsService } from "./doors/shared/doors.service"; 
import { DoorsDetailComponent } from "./doors/doors-detail/doors-detail.component"; 
import { DoorsAddComponent } from "./doors/doors-add/doors-add.component"; 
import { DoorsEditComponent } from "./doors/doors-edit/doors-edit.component"; 
import { DoorsSearchComponent } from "./doors/doors-search/doors-search.component"; 
import { doorsRouting } from "./doors/doors.routing"; 
import { EmplCleaRelationComponent } from "./emplclearelation/emplclearelation.component"; 
import { EmplCleaRelationService } from "./emplclearelation/shared/emplclearelation.service"; 
import { EmplCleaRelationDetailComponent } from "./emplclearelation/emplclearelation-detail/emplclearelation-detail.component"; 
import { EmplCleaRelationAddComponent } from "./emplclearelation/emplclearelation-add/emplclearelation-add.component"; 
import { EmplCleaRelationEditComponent } from "./emplclearelation/emplclearelation-edit/emplclearelation-edit.component"; 
import { EmplCleaRelationSearchComponent } from "./emplclearelation/emplclearelation-search/emplclearelation-search.component"; 
import { emplCleaRelationRouting } from "./emplclearelation/emplclearelation.routing"; 
import { CleaDoorRelationComponent } from "./cleadoorrelation/cleadoorrelation.component"; 
import { CleaDoorRelationService } from "./cleadoorrelation/shared/cleadoorrelation.service"; 
import { CleaDoorRelationDetailComponent } from "./cleadoorrelation/cleadoorrelation-detail/cleadoorrelation-detail.component"; 
import { CleaDoorRelationAddComponent } from "./cleadoorrelation/cleadoorrelation-add/cleadoorrelation-add.component"; 
import { CleaDoorRelationEditComponent } from "./cleadoorrelation/cleadoorrelation-edit/cleadoorrelation-edit.component"; 
import { CleaDoorRelationSearchComponent } from "./cleadoorrelation/cleadoorrelation-search/cleadoorrelation-search.component"; 
import { cleaDoorRelationRouting } from "./cleadoorrelation/cleadoorrelation.routing"; 


import { DynamicFormsModule } from '../../dynamicforms/dynamicforms.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
	employeesRouting, 
clearancesRouting, 
doorsRouting, 
emplCleaRelationRouting, 
cleaDoorRelationRouting, 

    DynamicFormsModule,
    
    

  ],
  declarations: [
    EmployeesComponent, 
EmployeesDetailComponent, 
EmployeesAddComponent, 
EmployeesEditComponent, 
EmployeesSearchComponent, 
ClearancesComponent, 
ClearancesDetailComponent, 
ClearancesAddComponent, 
ClearancesEditComponent, 
ClearancesSearchComponent, 
DoorsComponent, 
DoorsDetailComponent, 
DoorsAddComponent, 
DoorsEditComponent, 
DoorsSearchComponent, 
EmplCleaRelationComponent, 
EmplCleaRelationDetailComponent, 
EmplCleaRelationAddComponent, 
EmplCleaRelationEditComponent, 
EmplCleaRelationSearchComponent, 
CleaDoorRelationComponent, 
CleaDoorRelationDetailComponent, 
CleaDoorRelationAddComponent, 
CleaDoorRelationEditComponent, 
CleaDoorRelationSearchComponent, 

  ],
  exports: [
    EmployeesComponent, 
ClearancesComponent, 
DoorsComponent, 
EmplCleaRelationComponent, 
CleaDoorRelationComponent, 

  ],
  providers: [
    EmployeesService, 
ClearancesService, 
DoorsService, 
EmplCleaRelationService, 
CleaDoorRelationService, 

  ]
})
export class AcSecureModule { }
