import { Component, Input } from '@angular/core';
import {CommonsComponent} from "../../../common/commons.component";
import {default as config} from '../config/config.json';
import {ClearancesService} from "./shared/clearances.service";

@Component({
  selector: 'app-' + config['clearances'].component.nameModule.toLowerCase() ,
  templateUrl: './clearances.component.html',
  styleUrls: ['./clearances.component.css']
})
export class ClearancesComponent extends CommonsComponent {

  @Input() datafromadd: any[] = [];
  constructor(private clearancesService: ClearancesService) { 
    super(clearancesService);
    this.moduleName = config['clearances'].component.nameModule  ;
	this.modulePref = config['clearances'].component.prefix  ;
	this.keyName =  "_id";
  }
  ngOnInit() {
    
    super.ngOnInit(); 
  }
}
