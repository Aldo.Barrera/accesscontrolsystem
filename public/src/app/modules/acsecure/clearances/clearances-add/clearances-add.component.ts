﻿import { Component} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsAddComponent } from '../../../../common/commons-add/commons-add.component';

import {default as config} from '../../config/config.json';

import { ClearancesService } from '../shared/clearances.service';
import { FormsService } from '../../../../dynamicforms/forms/shared/forms.service';

@Component({
  selector: 'app-' + config['clearances'].component.nameModule.toLowerCase()  + '-addform',
  templateUrl: './clearances-add.component.html',
  styleUrls: ['./clearances-add.component.css'],
  providers:  [FormsService]
})
export class ClearancesAddComponent extends CommonsAddComponent {

  constructor(router: Router, route: ActivatedRoute, clearancesService: ClearancesService, formsService: FormsService) 
  {    
    super(router, route, clearancesService, formsService);    
    this.moduleName = config['clearances'].component.nameModule;
	this.modulePref = config['clearances'].component.prefix;
	this.keyName = "_id";
	this.components = config;
  }

  createForm() {
    this.controlsJson = config['clearances'].model;    
	this.components = config;
    super.createForm();    
  }
}
