﻿import { Routes, RouterModule } from '@angular/router';
import {default as config} from '../config/config.json';
import { ClearancesComponent } from './clearances.component';
import {ClearancesDetailComponent} from "./clearances-detail/clearances-detail.component";
import {ClearancesAddComponent} from "./clearances-add/clearances-add.component";
import {ClearancesEditComponent} from "./clearances-edit/clearances-edit.component";
import {ClearancesSearchComponent} from "./clearances-search/clearances-search.component";

const clearancesRoutes: Routes = [

    { path: config['clearances'].component.nameModule.toLowerCase() , component: ClearancesComponent, pathMatch: 'full' },
    { path: config['clearances'].component.nameModule.toLowerCase()  + '/:id', component: ClearancesDetailComponent},
    { path: config['clearances'].component.nameModule.toLowerCase()  + 'add', component: ClearancesAddComponent},
    { path: config['clearances'].component.nameModule.toLowerCase() + 'edit/:id', component: ClearancesEditComponent},
	{ path: config['clearances'].component.nameModule.toLowerCase() + 'search', component: ClearancesSearchComponent}
];

export const clearancesRouting = RouterModule.forChild(clearancesRoutes);
