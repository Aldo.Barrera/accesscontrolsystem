﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsDetailComponent } from '../../../../common/commons-detail/commons-detail.component';

import {default as config} from '../../config/config.json';
import { ClearancesService } from '../shared/clearances.service';

@Component({
  selector: 'app-' + config['clearances'].component.nameModule.toLowerCase() + '-form',
  templateUrl: './clearances-detail.component.html',
  styleUrls: ['./clearances-detail.component.css']
})
export class ClearancesDetailComponent extends CommonsDetailComponent {
  
  constructor(router: Router, route: ActivatedRoute, clearancesService: ClearancesService) 
  {
    super(router, route, clearancesService);
    this.moduleName = config['clearances'].component.nameModule ;
	this.modulePref = config['clearances'].component.prefix  ;
  }
}
