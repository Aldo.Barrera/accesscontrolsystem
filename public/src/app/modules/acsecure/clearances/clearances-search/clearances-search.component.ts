﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsSearchComponent } from '../../../../common/commons-search/commons-search.component';

import {default as config} from '../../config/config.json';
import { ClearancesService } from '../shared/clearances.service';

@Component({
  selector: 'app-' + config['clearances'].component.nameModule.toLowerCase() + '-form',
  templateUrl: './clearances-search.component.html',
  styleUrls: ['./clearances-search.component.css']
})
export class ClearancesSearchComponent extends CommonsSearchComponent {
  
  constructor(router: Router, route: ActivatedRoute, clearancesService: ClearancesService) 
  {
    super(router, route, clearancesService);
    this.moduleName = config['clearances'].component.nameModule ;
	this.modulePref = config['clearances'].component.prefix  ;
  }
}
