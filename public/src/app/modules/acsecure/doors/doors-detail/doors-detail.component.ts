﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsDetailComponent } from '../../../../common/commons-detail/commons-detail.component';

import {default as config} from '../../config/config.json';
import { DoorsService } from '../shared/doors.service';

@Component({
  selector: 'app-' + config['doors'].component.nameModule.toLowerCase() + '-form',
  templateUrl: './doors-detail.component.html',
  styleUrls: ['./doors-detail.component.css']
})
export class DoorsDetailComponent extends CommonsDetailComponent {
  
  constructor(router: Router, route: ActivatedRoute, doorsService: DoorsService) 
  {
    super(router, route, doorsService);
    this.moduleName = config['doors'].component.nameModule ;
	this.modulePref = config['doors'].component.prefix  ;
  }
}
