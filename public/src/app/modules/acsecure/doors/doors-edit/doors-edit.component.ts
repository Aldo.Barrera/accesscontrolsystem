﻿import { Component  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsEditComponent } from '../../../../common/commons-edit/commons-edit.component';

import {default as config} from '../../config/config.json';


import { DoorsService } from '../shared/doors.service';
import { FormsService } from '../../../../dynamicforms/forms/shared/forms.service';

@Component({
  selector: 'app-' + config['doors'].component.nameModule.toLowerCase()  + '-addform',
  templateUrl: './doors-edit.component.html',
  styleUrls: ['./doors-edit.component.css'],
  providers:  [FormsService]
})
export class DoorsEditComponent extends CommonsEditComponent {
 
  constructor(router: Router, route: ActivatedRoute, doorsService: DoorsService, formsService: FormsService) 
  {    
    super(router, route, doorsService, formsService);    
    this.moduleName = config['doors'].component.nameModule;
	this.modulePref = config['doors'].component.prefix  ;
	this.keyName = "_id"  ;
	this.components = config;
  }

  createForm() {
    this.controlsJson = config['doors'].model;
	  this.components = config;
    super.createForm();  
  }
  
}
