﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsSearchComponent } from '../../../../common/commons-search/commons-search.component';

import {default as config} from '../../config/config.json';
import { DoorsService } from '../shared/doors.service';

@Component({
  selector: 'app-' + config['doors'].component.nameModule.toLowerCase() + '-form',
  templateUrl: './doors-search.component.html',
  styleUrls: ['./doors-search.component.css']
})
export class DoorsSearchComponent extends CommonsSearchComponent {
  
  constructor(router: Router, route: ActivatedRoute, doorsService: DoorsService) 
  {
    super(router, route, doorsService);
    this.moduleName = config['doors'].component.nameModule ;
	this.modulePref = config['doors'].component.prefix  ;
  }
}
