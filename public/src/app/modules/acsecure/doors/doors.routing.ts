﻿import { Routes, RouterModule } from '@angular/router';
import {default as config} from '../config/config.json';
import { DoorsComponent } from './doors.component';
import {DoorsDetailComponent} from "./doors-detail/doors-detail.component";
import {DoorsAddComponent} from "./doors-add/doors-add.component";
import {DoorsEditComponent} from "./doors-edit/doors-edit.component";
import {DoorsSearchComponent} from "./doors-search/doors-search.component";

const doorsRoutes: Routes = [

    { path: config['doors'].component.nameModule.toLowerCase() , component: DoorsComponent, pathMatch: 'full' },
    { path: config['doors'].component.nameModule.toLowerCase()  + '/:id', component: DoorsDetailComponent},
    { path: config['doors'].component.nameModule.toLowerCase()  + 'add', component: DoorsAddComponent},
    { path: config['doors'].component.nameModule.toLowerCase() + 'edit/:id', component: DoorsEditComponent},
	{ path: config['doors'].component.nameModule.toLowerCase() + 'search', component: DoorsSearchComponent}
];

export const doorsRouting = RouterModule.forChild(doorsRoutes);
