import { Component, Input } from '@angular/core';
import {CommonsComponent} from "../../../common/commons.component";
import {default as config} from '../config/config.json';
import {DoorsService} from "./shared/doors.service";

@Component({
  selector: 'app-' + config['doors'].component.nameModule.toLowerCase() ,
  templateUrl: './doors.component.html',
  styleUrls: ['./doors.component.css']
})
export class DoorsComponent extends CommonsComponent {

  @Input() datafromadd: any[] = [];
  constructor(private doorsService: DoorsService) { 
    super(doorsService);
    this.moduleName = config['doors'].component.nameModule  ;
	this.modulePref = config['doors'].component.prefix  ;
	this.keyName =  "_id";
  }
  ngOnInit() {
    
    super.ngOnInit(); 
  }
}
