﻿import { Component} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsAddComponent } from '../../../../common/commons-add/commons-add.component';

import {default as config} from '../../config/config.json';

import { CleaDoorRelationService } from '../shared/cleadoorrelation.service';
import { FormsService } from '../../../../dynamicforms/forms/shared/forms.service';

@Component({
  selector: 'app-' + config['cleaDoorRelation'].component.nameModule.toLowerCase()  + '-addform',
  templateUrl: './cleadoorrelation-add.component.html',
  styleUrls: ['./cleadoorrelation-add.component.css'],
  providers:  [FormsService]
})
export class CleaDoorRelationAddComponent extends CommonsAddComponent {

  constructor(router: Router, route: ActivatedRoute, cleaDoorRelationService: CleaDoorRelationService, formsService: FormsService) 
  {    
    super(router, route, cleaDoorRelationService, formsService);    
    this.moduleName = config['cleaDoorRelation'].component.nameModule;
	this.modulePref = config['cleaDoorRelation'].component.prefix;
	this.keyName = "_id";
	this.components = config;
  }

  createForm() {
    this.controlsJson = config['cleaDoorRelation'].model;    
	this.components = config;
    super.createForm();    
  }
}
