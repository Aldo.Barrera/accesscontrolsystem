﻿import { Routes, RouterModule } from '@angular/router';
import {default as config} from '../config/config.json';
import { CleaDoorRelationComponent } from './cleadoorrelation.component';
import {CleaDoorRelationDetailComponent} from "./cleadoorrelation-detail/cleadoorrelation-detail.component";
import {CleaDoorRelationAddComponent} from "./cleadoorrelation-add/cleadoorrelation-add.component";
import {CleaDoorRelationEditComponent} from "./cleadoorrelation-edit/cleadoorrelation-edit.component";
import {CleaDoorRelationSearchComponent} from "./cleadoorrelation-search/cleadoorrelation-search.component";

const cleaDoorRelationRoutes: Routes = [

    { path: config['cleaDoorRelation'].component.nameModule.toLowerCase() , component: CleaDoorRelationComponent, pathMatch: 'full' },
    { path: config['cleaDoorRelation'].component.nameModule.toLowerCase()  + '/:id', component: CleaDoorRelationDetailComponent},
    { path: config['cleaDoorRelation'].component.nameModule.toLowerCase()  + 'add', component: CleaDoorRelationAddComponent},
    { path: config['cleaDoorRelation'].component.nameModule.toLowerCase() + 'edit/:id', component: CleaDoorRelationEditComponent},
	{ path: config['cleaDoorRelation'].component.nameModule.toLowerCase() + 'search', component: CleaDoorRelationSearchComponent}
];

export const cleaDoorRelationRouting = RouterModule.forChild(cleaDoorRelationRoutes);
