﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsDetailComponent } from '../../../../common/commons-detail/commons-detail.component';

import {default as config} from '../../config/config.json';
import { CleaDoorRelationService } from '../shared/cleadoorrelation.service';

@Component({
  selector: 'app-' + config['cleaDoorRelation'].component.nameModule.toLowerCase() + '-form',
  templateUrl: './cleadoorrelation-detail.component.html',
  styleUrls: ['./cleadoorrelation-detail.component.css']
})
export class CleaDoorRelationDetailComponent extends CommonsDetailComponent {
  
  constructor(router: Router, route: ActivatedRoute, cleaDoorRelationService: CleaDoorRelationService) 
  {
    super(router, route, cleaDoorRelationService);
    this.moduleName = config['cleaDoorRelation'].component.nameModule ;
	this.modulePref = config['cleaDoorRelation'].component.prefix  ;
  }
}
