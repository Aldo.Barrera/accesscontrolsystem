﻿import { Component  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsEditComponent } from '../../../../common/commons-edit/commons-edit.component';

import {default as config} from '../../config/config.json';


import { CleaDoorRelationService } from '../shared/cleadoorrelation.service';
import { FormsService } from '../../../../dynamicforms/forms/shared/forms.service';

@Component({
  selector: 'app-' + config['cleaDoorRelation'].component.nameModule.toLowerCase()  + '-addform',
  templateUrl: './cleadoorrelation-edit.component.html',
  styleUrls: ['./cleadoorrelation-edit.component.css'],
  providers:  [FormsService]
})
export class CleaDoorRelationEditComponent extends CommonsEditComponent {
 
  constructor(router: Router, route: ActivatedRoute, cleaDoorRelationService: CleaDoorRelationService, formsService: FormsService) 
  {    
    super(router, route, cleaDoorRelationService, formsService);    
    this.moduleName = config['cleaDoorRelation'].component.nameModule;
	this.modulePref = config['cleaDoorRelation'].component.prefix  ;
	this.keyName = "_id"  ;
	this.components = config;
  }

  createForm() {
    this.controlsJson = config['cleaDoorRelation'].model;
	  this.components = config;
    super.createForm();  
  }
  
}
