import { Component, Input } from '@angular/core';
import {CommonsComponent} from "../../../common/commons.component";
import {default as config} from '../config/config.json';
import {CleaDoorRelationService} from "./shared/cleadoorrelation.service";

@Component({
  selector: 'app-' + config['cleaDoorRelation'].component.nameModule.toLowerCase() ,
  templateUrl: './cleadoorrelation.component.html',
  styleUrls: ['./cleadoorrelation.component.css']
})
export class CleaDoorRelationComponent extends CommonsComponent {

  @Input() datafromadd: any[] = [];
  constructor(private cleaDoorRelationService: CleaDoorRelationService) { 
    super(cleaDoorRelationService);
    this.moduleName = config['cleaDoorRelation'].component.nameModule  ;
	this.modulePref = config['cleaDoorRelation'].component.prefix  ;
	this.keyName =  "_id";
  }
  ngOnInit() {
    
    super.ngOnInit(); 
  }
}
