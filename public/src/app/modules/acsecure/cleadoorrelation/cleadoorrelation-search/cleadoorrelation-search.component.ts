﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonsSearchComponent } from '../../../../common/commons-search/commons-search.component';

import {default as config} from '../../config/config.json';
import { CleaDoorRelationService } from '../shared/cleadoorrelation.service';

@Component({
  selector: 'app-' + config['cleaDoorRelation'].component.nameModule.toLowerCase() + '-form',
  templateUrl: './cleadoorrelation-search.component.html',
  styleUrls: ['./cleadoorrelation-search.component.css']
})
export class CleaDoorRelationSearchComponent extends CommonsSearchComponent {
  
  constructor(router: Router, route: ActivatedRoute, cleaDoorRelationService: CleaDoorRelationService) 
  {
    super(router, route, cleaDoorRelationService);
    this.moduleName = config['cleaDoorRelation'].component.nameModule ;
	this.modulePref = config['cleaDoorRelation'].component.prefix  ;
  }
}
