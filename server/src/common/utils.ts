const jwt = require('jsonwebtoken')
export abstract class CommonsUtil {



  public static generateToken(data) {
    return new Promise((resolve, reject) => {
      jwt.sign(
        {
            _id: data._id,
            ip: 1,
            dategenerate: Date.now()
            
        }, 
        'SeCrEtKeYfOrHaShInG', 
        {
        }, (err, token) => {
            if (err) reject(err)
            resolve(token) 
        })
      }
    )
  }


  public static decodeToken(token) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, 'SeCrEtKeYfOrHaShInG', (err, decoded) => {
        if(err) reject(err)
        resolve(decoded)
      })
    })
  }
  
}