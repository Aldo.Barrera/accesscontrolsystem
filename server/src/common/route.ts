import { Router, Request, Response, NextFunction } from 'express';
import keycloak from '../services/keycloack';
import * as App from "./socket";
export class CommonRouter extends App.CommonSocket{
  public router: Router
  public commonComponent: any
  public dataRoute: string
  public singleDataRoute: string 
  public dataRouteSearch: string   

  constructor() {
    super();
    this.router = Router();  
      
  }

  public addSocket(io:any) {
    super.setSocket(io);
    super.connect();
  }

  async GetAllData(req: Request, res: Response, next: NextFunction) {
    let query = req.query;
    let dataResponse;
    let restrict = query.fields?JSON.parse(query.fields):null;
    if (restrict){
      dataResponse = await this.commonComponent.GetAllRestrictData(restrict);
    } else if(query.populate) {
      dataResponse = await this.commonComponent.GetPopulateData();
    } else {
      dataResponse = await this.commonComponent.GetAllData();
    }
    res.send(dataResponse);
  }

  async FindData(req: Request, res: Response, next: NextFunction) {
    let query = req.query;
    const dataResponse = await this.commonComponent.FindData(query);
    res.send(dataResponse);
  }

  async GetSingleData(req: Request, res: Response, next: NextFunction) {    
    let id = req.params.id;
    let query = req.query;
    let dataResponse;
    if(query.populate) {
      dataResponse = await this.commonComponent.GetPopulateDataById(id);
    } else {
      dataResponse = await this.commonComponent.GetDataById(id);
    }
    res.send(dataResponse);
  }

  async InsertData(req: Request, res: Response, next: NextFunction) {
    let data = req.body;
    const dataResponse = await this.commonComponent.InsertData(data);
    if(dataResponse) {
      const dataResponsePopulate = await this.commonComponent.GetPopulateDataById(dataResponse.id);
      res.send(dataResponsePopulate);
    } else {
      res.send(dataResponse);
    } 
  }
  
  async UpdateData(req: Request, res: Response, next: NextFunction) {
    let data = req.body;
    let query = {[this.commonComponent.keyName]:req.params.id};
    const dataResponse = await this.commonComponent.UpdateData(query, data);
    if (dataResponse.n == 1 && dataResponse.ok == 1) {
      const dataResponsePopulate = await this.commonComponent.GetPopulateDataById(req.params.id);
      res.send(dataResponsePopulate);
    } else {
      res.send(dataResponse);
    }  
  }
  
  async DeleteData(req: Request, res: Response, next: NextFunction) {
    let query = {[this.commonComponent.keyName]:req.params.id};
    const dataResponse = await this.commonComponent.DeleteData(query);
    res.send(dataResponse);
  }
  
  public validateRole(token, req: Request, res: Response) {
    return true;
  }
  
  public init() {
    
    this.router.get(this.dataRoute, keycloak.getInstance().protect(this.validateRole.bind(this)),this.GetAllData.bind(this));    
    this.router.get(this.singleDataRoute, keycloak.getInstance().protect(this.validateRole.bind(this)), this.GetSingleData.bind(this));
	  this.router.get(this.dataRouteSearch, keycloak.getInstance().protect(this.validateRole.bind(this)), this.FindData.bind(this));
    this.router.post(this.dataRoute, keycloak.getInstance().protect(this.validateRole.bind(this)), this.InsertData.bind(this));
    this.router.put(this.singleDataRoute, keycloak.getInstance().protect(this.validateRole.bind(this)), this.UpdateData.bind(this));
    this.router.delete(this.singleDataRoute, keycloak.getInstance().protect(this.validateRole.bind(this)), this.DeleteData.bind(this));
  }
}