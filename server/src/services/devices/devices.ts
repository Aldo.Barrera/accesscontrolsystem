var configrequest = require('./configrequest');
const connect = require('./connect');
const axios = require('axios');
var qs = require('qs');

class Devices {

  constructor() {
     
  }

  public Connect() {
    return connect.connect();      
  }

  public SendToken(data: any, token:string)
  {

    return new Promise((resolve, reject) => {
      
      axios.put("https://" + data.door_ip + ":5000/api/v1/device/tokenUpdate", qs.stringify(data), configrequest).then(res => {
        resolve(res.data);
      }).catch(error => {
        
        resolve(error);
      });    
    });    
  }
  
  
 

}

export = new Devices();
