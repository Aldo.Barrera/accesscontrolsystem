const configrequest = require('./configrequest');
const axios = require('axios');

class Connect {
  
  constructor() {

  }
  public connect() {
    this.SetMethod('post');
    return this.GetToken();
  }

  public GetToken() {
    return new Promise((resolve, reject) => {
      configrequest.method = "POST";
      configrequest.cookie = null;
      configrequest.headers["Content-Type"] = "application/json";
      axios('/login/', configrequest).then(response => {
        if (response.headers['set-cookie'] && response.headers['set-cookie'].length >0)
          resolve(response.headers['set-cookie'][0]);   
        else
          resolve(null);       
      }).catch(error => {
          reject(error);
      });
    });
  }

  public SetMethod(method:string) {
    configrequest.method = method;
  }

}

export = new Connect();
