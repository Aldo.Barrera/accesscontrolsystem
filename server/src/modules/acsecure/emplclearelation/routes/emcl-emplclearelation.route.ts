import { Request, Response, NextFunction } from 'express';
import * as App from "../../../../common/route";
import {emclEmplCleaRelationComponent as EmclEmplCleaRelationComponent}  from "../emcl-emplclearelation.component";

export class EmclEmplCleaRelationRoute extends App.CommonRouter {

    constructor(emplCleaRelation:string, io?: any) {        
        super();
        this.dataRoute = "/emplclearelation/";
        this.singleDataRoute= "/emplclearelation/:id"; 
	this.dataRouteSearch = "/emplclearelationSearch/";
        this.commonComponent=EmclEmplCleaRelationComponent;
	this.moduleName  = emplCleaRelation;
        super.init();
        super.addSocket(io); 
    }
}

