import { EmplCleaRelationModel } from "./models/emplclearelation.model";
import * as App from "../../../common/component";

class EmclEmplCleaRelationComponent extends App.CommonComponent {
    constructor() {
        super();
        this.modelDb = EmplCleaRelationModel;
	this.namePref = EmplCleaRelationModel.namePref;
	this.name = EmplCleaRelationModel.name;
	this.keyName = EmplCleaRelationModel.keyName;
    }   
}
var emclEmplCleaRelationComponent = new EmclEmplCleaRelationComponent();
export {emclEmplCleaRelationComponent};
