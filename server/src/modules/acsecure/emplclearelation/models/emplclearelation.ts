'use strict';

import {ModelBase} from '../../../../config/databasemysql';
export default ModelBase.extend({
    tableName: 'emplCleaRelation',
    idAttribute: 'emcl_key',
});
