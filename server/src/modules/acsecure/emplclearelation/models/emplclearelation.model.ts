import { mongoose } from "../../../../config/database";
import { Document, Model, Schema } from "mongoose";
import * as App from "../../../../common/models/mongoose/model";
import {EmplCleaRelation} from './emcl-emplclearelation.model';

class EmclEmplCleaRelationModel extends App.CommonModel {
    constructor() {
        super();
        this.name = "emplCleaRelation";
	this.namePref = "emcl";
        this.model = EmplCleaRelation;
	this.keyName = "_id";
    }    
}


export const EmplCleaRelationModel = new EmclEmplCleaRelationModel();
