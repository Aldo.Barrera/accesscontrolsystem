import { mongoose } from "../../../../config/database";
import { Document, Model, Schema } from "mongoose";

var emplCleaRelation = {
    uscl_empl_id: { 
type:Schema.ObjectId, 
ref: "empl_employees", 
unique:false, 
required:true, 
}, 
uscl_clea_id: { 
type:Schema.ObjectId, 
ref: "clea_clearances", 
unique:false, 
required:true, 
}, 

}

var emplCleaRelationSchema = new Schema(emplCleaRelation);
export const EmplCleaRelation = mongoose.model("emcl_emplCleaRelation", emplCleaRelationSchema);
