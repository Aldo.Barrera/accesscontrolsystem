﻿import express from 'express';
import * as App from "../../common/module" 

import * as EmplEmployeesRoute from "./employees/routes/empl-employees.route" 
import * as CleaClearancesRoute from "./clearances/routes/clea-clearances.route" 
import * as DoorDoorsRoute from "./doors/routes/door-doors.route" 
import * as EmclEmplCleaRelationRoute from "./emplclearelation/routes/emcl-emplclearelation.route" 
import * as CldoCleaDoorRelationRoute from "./cleadoorrelation/routes/cldo-cleadoorrelation.route" 


class AcsAcSecureModule extends App.CommonModule {

    public addRoutes(appExpress:express.Application, io?: any) {  
		var emplEmployeesRoute = new EmplEmployeesRoute.EmplEmployeesRoute('employees', io); 
    appExpress.use('/api/v1/acsecure/', emplEmployeesRoute.router); 
var cleaClearancesRoute = new CleaClearancesRoute.CleaClearancesRoute('clearances', io); 
    appExpress.use('/api/v1/acsecure/', cleaClearancesRoute.router); 
var doorDoorsRoute = new DoorDoorsRoute.DoorDoorsRoute('doors', io); 
    appExpress.use('/api/v1/acsecure/', doorDoorsRoute.router); 
var emclEmplCleaRelationRoute = new EmclEmplCleaRelationRoute.EmclEmplCleaRelationRoute('emplCleaRelation', io); 
    appExpress.use('/api/v1/acsecure/', emclEmplCleaRelationRoute.router); 
var cldoCleaDoorRelationRoute = new CldoCleaDoorRelationRoute.CldoCleaDoorRelationRoute('cleaDoorRelation', io); 
    appExpress.use('/api/v1/acsecure/', cldoCleaDoorRelationRoute.router); 
 
    }
}
export default new AcsAcSecureModule()
