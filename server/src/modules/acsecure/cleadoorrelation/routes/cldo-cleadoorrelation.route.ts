import { Request, Response, NextFunction } from 'express';
import * as App from "../../../../common/route";
import {cldoCleaDoorRelationComponent as CldoCleaDoorRelationComponent}  from "../cldo-cleadoorrelation.component";

export class CldoCleaDoorRelationRoute extends App.CommonRouter {

    constructor(cleaDoorRelation:string, io?: any) {        
        super();
        this.dataRoute = "/cleadoorrelation/";
        this.singleDataRoute= "/cleadoorrelation/:id"; 
	this.dataRouteSearch = "/cleadoorrelationSearch/";
        this.commonComponent=CldoCleaDoorRelationComponent;
	this.moduleName  = cleaDoorRelation;
        super.init();
        super.addSocket(io); 
    }
}

