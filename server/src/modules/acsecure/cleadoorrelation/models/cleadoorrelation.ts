'use strict';

import {ModelBase} from '../../../../config/databasemysql';
export default ModelBase.extend({
    tableName: 'cleaDoorRelation',
    idAttribute: 'cldo_key',
});
