import { mongoose } from "../../../../config/database";
import { Document, Model, Schema } from "mongoose";

var cleaDoorRelation = {
    cldo_clea_id: { 
type:Schema.ObjectId, 
ref: "clea_clearances", 
unique:false, 
required:true, 
}, 
cldo_door_id: { 
type:Schema.ObjectId, 
ref: "door_doors", 
unique:false, 
required:true, 
}, 

}

var cleaDoorRelationSchema = new Schema(cleaDoorRelation);
export const CleaDoorRelation = mongoose.model("cldo_cleaDoorRelation", cleaDoorRelationSchema);
