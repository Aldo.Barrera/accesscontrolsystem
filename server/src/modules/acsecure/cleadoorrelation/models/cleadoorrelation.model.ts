import { mongoose } from "../../../../config/database";
import { Document, Model, Schema } from "mongoose";
import * as App from "../../../../common/models/mongoose/model";
import {CleaDoorRelation} from './cldo-cleadoorrelation.model';

class CldoCleaDoorRelationModel extends App.CommonModel {
    constructor() {
        super();
        this.name = "cleaDoorRelation";
	this.namePref = "cldo";
        this.model = CleaDoorRelation;
	this.keyName = "_id";
    }    
}


export const CleaDoorRelationModel = new CldoCleaDoorRelationModel();
