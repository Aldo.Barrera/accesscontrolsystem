import { CleaDoorRelationModel } from "./models/cleadoorrelation.model";
import * as App from "../../../common/component";

class CldoCleaDoorRelationComponent extends App.CommonComponent {
    constructor() {
        super();
        this.modelDb = CleaDoorRelationModel;
	this.namePref = CleaDoorRelationModel.namePref;
	this.name = CleaDoorRelationModel.name;
	this.keyName = CleaDoorRelationModel.keyName;
    }   
}
var cldoCleaDoorRelationComponent = new CldoCleaDoorRelationComponent();
export {cldoCleaDoorRelationComponent};
