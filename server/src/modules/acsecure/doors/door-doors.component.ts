import { DoorsModel } from "./models/doors.model";
import { emplEmployeesComponent as EmplEmployeesComponent } from "../employees/empl-employees.component";
import {emclEmplCleaRelationComponent as EmclEmplCleaRelationComponent}  from "../emplclearelation/emcl-emplclearelation.component";
import { cldoCleaDoorRelationComponent as CldoCleaDoorRelationComponent} from "../cleadoorrelation/cldo-cleadoorrelation.component";
import * as App from "../../../common/component";
const Devices = require('../../../services/devices/devices');

import * as Utils from "../../../common/utils";

class DoorDoorsComponent extends App.CommonComponent {
    constructor() {
        super();
        this.modelDb = DoorsModel;
	this.namePref = DoorsModel.namePref;
	this.name = DoorsModel.name;
	this.keyName = DoorsModel.keyName;
    }   
    
    async SendToken() {
        let dataResponses = [];
        let validDoors = await this.FindData({door_blocked:false});
        
        for(let door of validDoors) {
            let token = await Utils.CommonsUtil.generateToken(door);
            let dataIp1 = {
                door_ip: door.door_ip1,
                took_code: token
            }
            try {
                let dataResponse = await Devices.SendToken(dataIp1);
                dataResponses.push(dataResponse);
            } catch(ex) {
                dataResponses.push({error:"Fallo puerta : " + door.door_ip1});
            }
            
        }

        return dataResponses;
    }

    async TokenValidate(token) {
            try {
                var  dataResponse = await Utils.CommonsUtil.decodeToken(token);
                let door = await super.FindOne({_id:dataResponse});
                let doorResult = {
                    door_name: door.door_name,
                    door_to: door.door_to,
                    date: dataResponse,
                    id: door._id
                }
                return doorResult;
            } catch(ex) {
                return {error:"Fallo en el token : "};
            }
    }

    async UserValidate(user_email, door_id) {
        try {
           var valid = false;
            let employee = await EmplEmployeesComponent.FindOne({empl_email:user_email});
            let clearances = await EmclEmplCleaRelationComponent.FindData({uscl_empl_id:employee.id});
            for(let clearance of clearances) {
                let doorsAllowed = await CldoCleaDoorRelationComponent.FindData({cldo_clea_id:clearance.uscl_clea_id});
                for(let validDoor of doorsAllowed) {
                    if (validDoor.cldo_door_id.toString() == door_id.id.toString())
                        valid = true; 
                }
            }
            
            return valid;
        } catch(ex) {
            return {error:"Fallo en el token : "};
        }
    }
}
var doorDoorsComponent = new DoorDoorsComponent();
export {doorDoorsComponent};
