import { mongoose } from "../../../../config/database";
import { Document, Model, Schema } from "mongoose";
import * as App from "../../../../common/models/mongoose/model";
import {Doors} from './door-doors.model';

class DoorDoorsModel extends App.CommonModel {
    constructor() {
        super();
        this.name = "doors";
	this.namePref = "door";
        this.model = Doors;
	this.keyName = "_id";
    }    
}


export const DoorsModel = new DoorDoorsModel();
