import { mongoose } from "../../../../config/database";
import { Document, Model, Schema } from "mongoose";

var doors = {
    door_name: { 
type:String, 
unique:true, 
required:true, 
}, 
door_from: { 
type:String, 
unique:false, 
required:true, 
}, 
door_to: { 
type:String, 
unique:false, 
required:true, 
}, 
door_ip1: { 
type:String, 
unique:true, 
required:true, 
}, 
door_ip2: { 
type:String, 
unique:true, 
required:undefined, 
}, 
door_blocked: { 
type:Boolean, 
unique:false, 
required:true, 
}, 

}

var doorsSchema = new Schema(doors);
export const Doors = mongoose.model("door_doors", doorsSchema);
