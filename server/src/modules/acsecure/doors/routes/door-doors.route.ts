import { Request, Response, NextFunction } from 'express';
import * as App from "../../../../common/route";
import {doorDoorsComponent as DoorDoorsComponent}  from "../door-doors.component";

export class DoorDoorsRoute extends App.CommonRouter {
	public dataRouteSent;
    public dataRouteTokenValidate;
    constructor(doors:string, io?: any) {        
        super();
        this.dataRoute = "/doors/";
        this.singleDataRoute= "/doors/:id"; 
		    this.dataRouteSearch = "/doorsSearch/";
		    this.dataRouteSent = "/sendtoken/";
        this.dataRouteTokenValidate = "/tokenvalidate/";
        this.commonComponent=DoorDoorsComponent;
		    this.moduleName  = doors;
        this.init();
        super.addSocket(io); 
    }
    async SendToken(req: Request, res: Response, next: NextFunction) {
      let response = await DoorDoorsComponent.SendToken();
      res.send(response);
    }

    async TokenValidate(req: Request, res: Response, next: NextFunction) {
      let token = req.body.took_code;
      let user_email = req.body.user_email;
      let response = await DoorDoorsComponent.TokenValidate(token);
      let isValid =  await DoorDoorsComponent.UserValidate(user_email, response);
      let result = {
        door:response,
        isValid: isValid
      }
      res.send(result);
    }

    public init() {    
      super.init();
      this.router.get(this.dataRouteSent,  this.SendToken);
      this.router.put(this.dataRouteTokenValidate,  this.TokenValidate);
      
    }
}

