import { Request, Response, NextFunction } from 'express';
import * as App from "../../../../common/route";
import {cleaClearancesComponent as CleaClearancesComponent}  from "../clea-clearances.component";

export class CleaClearancesRoute extends App.CommonRouter {

    constructor(clearances:string, io?: any) {        
        super();
        this.dataRoute = "/clearances/";
        this.singleDataRoute= "/clearances/:id"; 
	this.dataRouteSearch = "/clearancesSearch/";
        this.commonComponent=CleaClearancesComponent;
	this.moduleName  = clearances;
        super.init();
        super.addSocket(io); 
    }
}

