import { ClearancesModel } from "./models/clearances.model";
import * as App from "../../../common/component";

class CleaClearancesComponent extends App.CommonComponent {
    constructor() {
        super();
        this.modelDb = ClearancesModel;
	this.namePref = ClearancesModel.namePref;
	this.name = ClearancesModel.name;
	this.keyName = ClearancesModel.keyName;
    }   
}
var cleaClearancesComponent = new CleaClearancesComponent();
export {cleaClearancesComponent};
