import { mongoose } from "../../../../config/database";
import { Document, Model, Schema } from "mongoose";
import * as App from "../../../../common/models/mongoose/model";
import {Clearances} from './clea-clearances.model';

class CleaClearancesModel extends App.CommonModel {
    constructor() {
        super();
        this.name = "clearances";
	this.namePref = "clea";
        this.model = Clearances;
	this.keyName = "_id";
    }    
}


export const ClearancesModel = new CleaClearancesModel();
