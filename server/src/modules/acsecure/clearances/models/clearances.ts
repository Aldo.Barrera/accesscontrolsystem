'use strict';

import {ModelBase} from '../../../../config/databasemysql';
export default ModelBase.extend({
    tableName: 'clearances',
    idAttribute: 'clea_key',
});
