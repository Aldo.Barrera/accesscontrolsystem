import { mongoose } from "../../../../config/database";
import { Document, Model, Schema } from "mongoose";

var clearances = {
    clea_name: { 
type:String, 
unique:true, 
required:true, 
}, 
clea_color: { 
type:String, 
unique:false, 
required:true, 
}, 
clea_status: { 
type:Boolean, 
unique:false, 
required:true, 
}, 

}

var clearancesSchema = new Schema(clearances);
export const Clearances = mongoose.model("clea_clearances", clearancesSchema);
