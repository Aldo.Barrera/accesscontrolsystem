import { EmployeesModel } from "./models/employees.model";
import * as App from "../../../common/component";

class EmplEmployeesComponent extends App.CommonComponent {
    constructor() {
        super();
        this.modelDb = EmployeesModel;
	this.namePref = EmployeesModel.namePref;
	this.name = EmployeesModel.name;
	this.keyName = EmployeesModel.keyName;
    }   
}
var emplEmployeesComponent = new EmplEmployeesComponent();
export {emplEmployeesComponent};
