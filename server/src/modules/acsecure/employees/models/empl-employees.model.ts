import { mongoose } from "../../../../config/database";
import { Document, Model, Schema } from "mongoose";

var employees = {
    empl_firstname: { 
type:String, 
unique:false, 
required:true, 
}, 
empl_lastname: { 
type:String, 
unique:false, 
required:true, 
}, 
empl_ci: { 
type:String, 
unique:false, 
required:true, 
}, 
empl_email: { 
type:String, 
unique:true, 
required:true, 
}, 
empl_image: { 
type:String, 
unique:false, 
required:false, 
}, 
empl_status: { 
type:Boolean, 
unique:false, 
required:true, 
}, 

}

var employeesSchema = new Schema(employees);
export const Employees = mongoose.model("empl_employees", employeesSchema);
