'use strict';

import {ModelBase} from '../../../../config/databasemysql';
export default ModelBase.extend({
    tableName: 'employees',
    idAttribute: 'empl_key',
});
