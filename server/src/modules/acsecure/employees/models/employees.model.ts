import { mongoose } from "../../../../config/database";
import { Document, Model, Schema } from "mongoose";
import * as App from "../../../../common/models/mongoose/model";
import {Employees} from './empl-employees.model';

class EmplEmployeesModel extends App.CommonModel {
    constructor() {
        super();
        this.name = "employees";
	this.namePref = "empl";
        this.model = Employees;
	this.keyName = "_id";
    }    
}


export const EmployeesModel = new EmplEmployeesModel();
