import { Request, Response, NextFunction } from 'express';
import * as App from "../../../../common/route";
import {emplEmployeesComponent as EmplEmployeesComponent}  from "../empl-employees.component";

export class EmplEmployeesRoute extends App.CommonRouter {

    constructor(employees:string, io?: any) {        
        super();
        this.dataRoute = "/employees/";
        this.singleDataRoute= "/employees/:id"; 
	this.dataRouteSearch = "/employeesSearch/";
        this.commonComponent=EmplEmployeesComponent;
	this.moduleName  = employees;
        super.init();
        super.addSocket(io); 
    }
}

