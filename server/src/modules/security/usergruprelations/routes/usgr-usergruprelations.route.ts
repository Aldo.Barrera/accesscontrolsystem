import { Request, Response, NextFunction } from 'express';
import * as App from "../../../../common/route";
import {usgrUserGrupRelationsComponent as UsgrUserGrupRelationsComponent}  from "../usgr-usergruprelations.component";

export class UsgrUserGrupRelationsRoute extends App.CommonRouter {

    constructor(userGrupRelations:string, io?: any) {        
        super();
        this.dataRoute = "/usergruprelations/";
        this.singleDataRoute= "/usergruprelations/:id"; 
	this.dataRouteSearch = "/usergruprelationsSearch/";
        this.commonComponent=UsgrUserGrupRelationsComponent;
	this.moduleName  = userGrupRelations;
        super.init();
        super.addSocket(io); 
    }
}

