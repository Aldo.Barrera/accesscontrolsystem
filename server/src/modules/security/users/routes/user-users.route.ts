import { Request, Response, NextFunction } from 'express';
import * as App from "../../../../common/route";
import {userUsersComponent as UserUsersComponent}  from "../user-users.component";
import keycloak from '../../../../services/keycloack';
export class UserUsersRoute extends App.CommonRouter {
    loginRoute:string;
    constructor(users:string, io?: any) {        
        super();
        this.dataRoute = "/users/";
        this.singleDataRoute= "/users/:id"; 
        this.dataRouteSearch = "/usersSearch/";
        this.loginRoute = "/login/";
        this.commonComponent=UserUsersComponent;
	    this.moduleName  = users;
        this.init();
        super.addSocket(io); 
    }

    async login(req: Request, res: Response, next: NextFunction) {

        try {
            let data = req.body;
        
            const dataResponse = await keycloak.getToken(data.username, data.password);
           
            res.send(dataResponse);
        } catch(error){
            res.send(error);
        }
       
         
      }

    public init() {
        super.init();
        this.router.post(this.loginRoute, this.login.bind(this));    

    }

}

