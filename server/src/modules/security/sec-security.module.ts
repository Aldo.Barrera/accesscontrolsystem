﻿import express from 'express';
import * as App from "../../common/module" 

import * as ApigApiGeneralsRoute from "./apigenerals/routes/apig-apigenerals.route" 
import * as ApisApiSpecificsRoute from "./apispecifics/routes/apis-apispecifics.route" 
import * as GagpGrupApigPrivileguesRoute from "./grupapigprivilegues/routes/gagp-grupapigprivilegues.route" 
import * as GaspGrupApisPrivileguesRoute from "./grupapisprivilegues/routes/gasp-grupapisprivilegues.route" 
import * as GrupGroupsRoute from "./groups/routes/grup-groups.route" 
import * as UserUsersRoute from "./users/routes/user-users.route" 
import * as UsgrUserGrupRelationsRoute from "./usergruprelations/routes/usgr-usergruprelations.route" 


class SecSecurityModule extends App.CommonModule {

    public addRoutes(appExpress:express.Application, io?: any) {  
		var apigApiGeneralsRoute = new ApigApiGeneralsRoute.ApigApiGeneralsRoute('apiGenerals', io); 
    appExpress.use('/api/v1/security/', apigApiGeneralsRoute.router); 
var apisApiSpecificsRoute = new ApisApiSpecificsRoute.ApisApiSpecificsRoute('apiSpecifics', io); 
    appExpress.use('/api/v1/security/', apisApiSpecificsRoute.router); 
var gagpGrupApigPrivileguesRoute = new GagpGrupApigPrivileguesRoute.GagpGrupApigPrivileguesRoute('grupApigPrivilegues', io); 
    appExpress.use('/api/v1/security/', gagpGrupApigPrivileguesRoute.router); 
var gaspGrupApisPrivileguesRoute = new GaspGrupApisPrivileguesRoute.GaspGrupApisPrivileguesRoute('grupApisPrivilegues', io); 
    appExpress.use('/api/v1/security/', gaspGrupApisPrivileguesRoute.router); 
var grupGroupsRoute = new GrupGroupsRoute.GrupGroupsRoute('groups', io); 
    appExpress.use('/api/v1/security/', grupGroupsRoute.router); 
var userUsersRoute = new UserUsersRoute.UserUsersRoute('users', io); 
    appExpress.use('/api/v1/security/', userUsersRoute.router); 
var usgrUserGrupRelationsRoute = new UsgrUserGrupRelationsRoute.UsgrUserGrupRelationsRoute('userGrupRelations', io); 
    appExpress.use('/api/v1/security/', usgrUserGrupRelationsRoute.router); 
 
    }
}
export default new SecSecurityModule()
