import { Request, Response, NextFunction } from 'express';
import * as App from "../../../../common/route";
import {apisApiSpecificsComponent as ApisApiSpecificsComponent}  from "../apis-apispecifics.component";

export class ApisApiSpecificsRoute extends App.CommonRouter {

    constructor(apiSpecifics:string, io?: any) {        
        super();
        this.dataRoute = "/apispecifics/";
        this.singleDataRoute= "/apispecifics/:id"; 
	this.dataRouteSearch = "/apispecificsSearch/";
        this.commonComponent=ApisApiSpecificsComponent;
	this.moduleName  = apiSpecifics;
        super.init();
        super.addSocket(io); 
    }
}

