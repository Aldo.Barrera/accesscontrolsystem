import { Request, Response, NextFunction } from 'express';
import * as App from "../../../../common/route";
import {gagpGrupApigPrivileguesComponent as GagpGrupApigPrivileguesComponent}  from "../gagp-grupapigprivilegues.component";

export class GagpGrupApigPrivileguesRoute extends App.CommonRouter {

    constructor(grupApigPrivilegues:string, io?: any) {        
        super();
        this.dataRoute = "/grupapigprivilegues/";
        this.singleDataRoute= "/grupapigprivilegues/:id"; 
	this.dataRouteSearch = "/grupapigprivileguesSearch/";
        this.commonComponent=GagpGrupApigPrivileguesComponent;
	this.moduleName  = grupApigPrivilegues;
        super.init();
        super.addSocket(io); 
    }
}

