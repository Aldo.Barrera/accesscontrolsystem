import { Request, Response, NextFunction } from 'express';
import * as App from "../../../../common/route";
import {grupGroupsComponent as GrupGroupsComponent}  from "../grup-groups.component";

export class GrupGroupsRoute extends App.CommonRouter {

    constructor(groups:string, io?: any) {        
        super();
        this.dataRoute = "/groups/";
        this.singleDataRoute= "/groups/:id"; 
	this.dataRouteSearch = "/groupsSearch/";
        this.commonComponent=GrupGroupsComponent;
	this.moduleName  = groups;
        super.init();
        super.addSocket(io); 
    }
}

