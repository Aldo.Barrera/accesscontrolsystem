import * as path from 'path';
import express from 'express';
import * as bodyParser from 'body-parser';

import * as log4js from 'log4js';

import log4jsconfig from './config/log4js.json';
//var log4jsconfig = require('./config/log4js.json');
log4js.configure(log4jsconfig);

import keycloak from './services/keycloack';
var schedule = require('node-schedule');
//var keycloak = require('./services/keycloack');

import SecSecurityModule from "./modules/security/sec-security.module" 
import AcsAcSecureModule from "./modules/acsecure/acs-acsecure.module" 
const DoorDoorsComponent = require('./modules/acsecure/doors/door-doors.component');




//var cors = require('cors');
// Creates and configures an ExpressJS web server.
class App {

  // ref to Express instance
  public express: express.Application;
  public socket;
  private io;

  //Run configuration methods on the Express instance.
  constructor() {
    this.express = express();
    this.middleware();
    this.Security();
    this.schedulaeTask();
  }

  // Configure Express middleware.
  private middleware(): void {

    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    //this.express.use(cors({origin: 'http://localhost:4200'}));

    this.express.use(function (req, res, next) {
      let httpRequest = new Date() + req.method + req.url;
      console.log(httpRequest);
      next();
    });
  }

  // Configure Security.
  private Security(): void {

    this.express.use(keycloak.sessionConfig);
    this.express.use(keycloak.getInstance().middleware());
  }

  //Configure Sockets
  public addSocketIO(io: any): void {
    this.io = io;
    this.routes();

  }

  
  // Configure API endpoints.
  private routes(): void {

	let router = express.Router();
	router.use(express.static(path.join(__dirname, '../../public/dist/public')));
    this.express.use('/', router);
    this.express.use(log4js.connectLogger(log4js.getLogger("http"), { level: 'auto' }));
    this.express.use('/errorpermission', router); 

	SecSecurityModule.addRoutes(this.express, this.io); 
AcsAcSecureModule.addRoutes(this.express, this.io); 


    this.express.get('*', (req, res) => {
      res.sendFile(path.join(__dirname, '../../public/dist/public/index.html'));
    });
    
  }
  private schedulaeTask(): void {

    var j = schedule.scheduleJob('20 * * * *', function(){
      var today = new Date();
      console.log('----TIME-----');
      console.log(today);
      console.log('The clean reports device report is executing');
      var CB = function (output: string) {
        console.log('The clean reports device was executed');
      }
      DoorDoorsComponent.SendTokens(CB);
    });

    
  }

}

export default new App();
