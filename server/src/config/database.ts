
import mongoose from "mongoose";

var database = process.env.DATABASE || "127.0.0.1";
var port = process.env.DATABASE_PORT || "27017";

(mongoose as any).Promise = global.Promise;

  mongoose.connect("mongodb://" + database +":" + port +"/accesscontrolsystem", {
    useNewUrlParser: true,
    useCreateIndex: true
  });

export { mongoose };
